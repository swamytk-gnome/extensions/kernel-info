/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* Import all necessary modules */
const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const GObject = imports.gi.GObject;
const GLib = imports.gi.GLib
const Gio = imports.gi.Gio;
const Shell = imports.gi.Shell;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const Config = imports.misc.config;

var FreezableBinLayout = GObject.registerClass(
class FreezableBinLayout extends Clutter.BinLayout {
    _init() {
        super._init();

        this._frozen = false;
        this._savedWidth = [NaN, NaN];
        this._savedHeight = [NaN, NaN];
    }

    set frozen(v) {
        if (this._frozen == v)
            return;

        this._frozen = v;
        if (!this._frozen)
            this.layout_changed();
    }

    vfunc_get_preferred_width(container, forHeight) {
        if (!this._frozen || this._savedWidth.some(isNaN))
            return super.vfunc_get_preferred_width(container, forHeight);
        return this._savedWidth;
    }

    vfunc_get_preferred_height(container, forWidth) {
        if (!this._frozen || this._savedHeight.some(isNaN))
            return super.vfunc_get_preferred_height(container, forWidth);
        return this._savedHeight;
    }

    vfunc_allocate(container, allocation) {
        super.vfunc_allocate(container, allocation);

        let [width, height] = allocation.get_size();
        this._savedWidth = [width, width];
        this._savedHeight = [height, height];
    }
});

class Extension {
    constructor() {
		this._indicator = null;
    }

    enable() {
		log(`enabling ${Me.metadata.name}`);

		let hbox;
        let indicatorName = `${Me.metadata.name} Indicator`;
        
        // Create a panel button
        this._indicator = new PanelMenu.Button(0.0, indicatorName, false);

        /* Add an icon to indicator */
	    this.kernelIcon = Gio.icon_new_for_string(`${Me.path}/icons/tux.png`);
        let icon = new St.Icon({
            gicon: this.kernelIcon,
            style_class: 'system-status-icon'
        });
		this._indicator.add_actor(icon);

		/* Add a box layout to indicator */
        let layout = new FreezableBinLayout();
        let bin = new St.Widget({ layout_manager: layout });
        // For some minimal compatibility with PopupMenuItem
        bin._delegate = this;
        this._indicator.menu.box.add_child(bin);

		/* Add content to box */
		hbox = new St.BoxLayout({name: 'kernelData' });
		bin.add_actor(hbox);

		let kernel_info = GLib.spawn_command_line_sync("uname -rio")[1].toString();
		let os_release = Shell.get_file_contents_utf8_sync("/etc/os-release").toString();
	    let text = new St.Label({
    	    text : "Kernel Info:\n" + "============\n" + 
				kernel_info + "\n\nLinux Distro Info:\n" + 
				"==================\n" + os_release,
        	y_align : Clutter.ActorAlign.CENTER
	    });
		hbox.add_actor(text);

        // `Main.panel` is the actual panel you see at the top of the screen,
        // not a class constructor.
        Main.panel.addToStatusArea(indicatorName, this._indicator);	
    }

    // REMINDER: It's required for extensions to clean up after themselves when
    // they are disabled. This is required for approval during review!
    disable() {
        log(`disabling ${Me.metadata.name}`);

        this._indicator.destroy();
        this._indicator = null;
    }
}

function init() {
	log(`initializing ${Me.metadata.name}`);
    return new Extension();
}

